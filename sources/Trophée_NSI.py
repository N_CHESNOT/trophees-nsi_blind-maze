import pyxel
from math import sqrt
import time

# taille de la carte du jeu
x_fenetre = 512
y_fenetre = 512



#on initialise la fenetre
pyxel.init(256, 256, title="Trophée NSI", display_scale=2)


# on lis la sauvegarde sur la progression du joueur
sauvegarde = open("save.txt", "r")

#sauvegarde.readline().replace("\n", "")

#on charge les images et la sauvegarde
pyxel.load("1.pyxres")




# coordonnée de la caméra
x_camera = 0
y_camera = 0
camera = [ x_camera , y_camera ]


# information sur le nombre de niveau

niveau = 0

nb_niveau = 3


# position initiale
joueur_x = [ 35  , 12, 35 ]
joueur_y = [ 20  , 484, 20 ]



# Coordonnées joueur et état
joueur = [ [ 35 , 20 ], [ 12 , 484 ], [ 35 , 20 ] ]

en_mouvement, direction_j =  False, "droite"



# Vitesse : joueur et ennemis; Taille :joueur x ennemis, obstacle et les tiles; Coordonnées de l'objectif et de banque d'image pour le personnage; l'état des menus.

v_j, v_e = 3, 0

taille, taille_obstacle, taille_tile = 8, 16, 8

objectif = [ [ 468, 500 ], [484, 484], [484, 484] ]

objectifs_atteint = [True, False, False, False, False, False, False, False]

u, v = 0, 16

menu, menu_demarrage, menu_niveau = False, True, False

speedrun_mode, t_start, t_end = False, False, False
meilleur_temps = [0, 0, 0]

admin = False

direction = ""



dialogue_tuto = ["? - Enfin reveiller, il fait sombre ici.", "? - Commencons par les bases, une fois ta torche ""\n" "\n""allume tu appuyeras sur les fleches du haut, gauche, ""\n" "\n""bas, droite ou Z Q S D pour te deplacer.", "? - Le temps presse, essaye de trouver ""\n" "\n""le portail qui te ramenera a nous..", "? - Mais fait attention, des robots bloques les ""\n" "\n""passages donc evite d'etre dans leur champ de vision", "? - Ah, et une derniere choses, essaye de recuperer ""\n" "\n""des essences je t'en expliquerais l'utilites ""\n" "\n""plus tard...","" ]
dialogue_niveau2 = ["Tu es venu a bout du premier niveau !","Mais attention on ne frotte que le bout de l'iceberg, ""\n" "\n""les prochaines salles regorgeront de plus de gardes", ""]

dialogue_niveau3 = ["Tu viens de sortir du premier etage du labyrinthe ""\n" "\n""mais ce n’est que le debut. ","Desormais, les labyrinthes se complexifient et tu ""\n""\n""n'es pas a l'abri d'apparition de nouveaux gardes, ""\n""\n""plus dangereux encore, que ceux que tu as vus ""\n""\n""jusque-ici…",""]
dialogue_n = 0

dialogue_tuto_confirmations = [""]



# les bonus et si ils sont récupérés
bonus = [ [ [300, 116], [168, 128], [104, 12 ], [332, 300], [444, 354], [52 , 244], [428, 76 ] ],
          [ [52 , 180], [84 , 132], [196, 164], [244, 68 ], [212, 420], [484, 292], [428, 20 ] ],
          [ [100, 32 ] ] ]

bonus_recup = [[False, False, False, False, False, False, False],
               [False, False, False, False, False, False, False],
               [False, ]]

total_b = [[0], [0], [0], [0], [0], [0]]


sauvegarde.close()



# les différents ennemies (entrer futur cordonné d'ennemie)
ennemis_i = [ [ [ 100 , 68  ], [ 145 , 68  ], [ 204 , 196 ], [ 100 , 293 ], [ 308 , 20  ], [ 500 , 116 ], [ 104 , 436 ], [ 388 , 252 ], [ 4   , 196 ], [ 252 , 292 ], [ 372 , 244 ] ],
              [ [ 12  , 306 ], [ 92  , 436 ], [ 4   , 212 ], [ 68  , 68  ], [ 352 , 116 ], [ 452 , 67  ], [ 340 , 20  ], [ 245 , 308 ], [ 420 , 227 ], [ 490 , 388 ], [ 380 , 491 ], [ 140 , 341 ], [ 243 , 196 ] ],
          [  [ 12  , 306 ],]]      # ennemis initiale

ennemis =   [ [ [ 100 , 68  ], [ 145 , 68  ], [ 204 , 196 ], [ 100 , 293 ], [ 308 , 20  ], [ 500 , 116 ], [ 104 , 436 ], [ 388 , 252 ], [ 4   , 196 ], [ 252 , 292 ], [ 372 , 244 ] ],
              [ [ 12  , 306 ], [ 92  , 436 ], [ 4   , 212 ], [ 68  , 68  ], [ 352 , 116 ], [ 452 , 67  ], [ 340 , 20  ], [ 245 , 308 ], [ 420 , 227 ], [ 490 , 388 ], [ 380 , 491 ], [ 140 , 341 ], [ 243 , 196 ]  ],
              [  [ 12  , 306 ] ]]   # coordonnées ennemis

targets =   [ [ [ 100 , 260 ], [ 265 , 68  ], [ 372 , 196 ], [ 100 , 431 ], [ 500 , 20  ], [ 308 , 116 ], [ 380 , 436 ], [ 388 , 498 ], [ 4   , 340 ], [ 252 , 430 ], [ 498 , 244 ] ],
              [ [ 12  , 438 ], [ 92  , 304 ], [ 148 , 212 ], [ 188 , 68  ], [ 148 , 116 ], [ 452 , 229 ], [ 340 , 116 ], [ 419 , 308 ], [ 420 , 389 ], [ 358 , 388 ], [ 380 , 389 ], [ 140 , 467 ], [ 357 , 196 ] ],
               [ [ 12  , 306 ]]  ]   # objectif des ennemis

etat = [ [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0] ]     # état de si les ennemis ont atteints leur targets (0 : non; 1 : oui ou partielle; 2 : oui mais code pas adapter pour le moment

direction_e = [ ["","","","","","","","","","",""],
                ["","","","","","","","","","","","",""],
                [""] ]




# les obstacles (mur) dans le jeu [illisible si la page n'est pas étendu sur 2 écrans]
obstacles =[[                                         [80, 0  ],                                                                                                                                    [272, 0  ],
                                                      [80, 16 ],                                                                                                                                    [272, 16 ],
                                                      [80, 32 ], [96, 32 ], [112, 32 ], [128, 32 ],                                                                                                 [272, 32 ],                                     [328, 40 ], [344, 40 ],               [376, 40 ], [392, 40 ], [408, 40 ], [424, 40 ], [440, 40 ],      [456, 40 ], [472, 40 ],
                                                                                        [128, 48 ],                                                                                                 [272, 48 ],                                     [328, 56 ], [344, 56 ],               [376, 56 ], [392, 56 ], [408, 56 ], [424, 56 ], [440, 56 ],      [456, 56 ], [472, 56 ],
                                                                                                                                                                                                                                                    [328, 72 ], [344, 72 ], [360, 72 ],   [376, 72 ], [392, 72 ], [408, 72 ],                              [456, 72 ], [472, 72 ],
                                                                                        [128, 80 ],                                                                                                 [272, 80 ], [288, 80], [304, 80], [312, 80],    [328, 88 ], [344, 88 ], [360, 88 ],   [376, 88 ], [392, 88 ], [408, 88 ], [424, 88 ],                  [456, 88 ], [472, 88 ],
          [16, 96 ], [32, 96 ], [48, 96 ], [64, 96 ],                                   [128, 96 ], [144, 96 ], [160, 96 ], [176, 96 ], [192, 96 ], [208, 96 ], [224, 96 ], [240, 96 ], [256, 96 ], [272, 96 ], [288, 88], [304, 88], [312, 88],
          [16, 112], [32, 112], [48, 112], [64, 112],                                   [128, 112],                                                 [208, 112],     [232, 120], [248, 120],         [272, 112],
          [16, 128], [32, 128], [48, 128], [64, 128],                                   [128, 128],                                                 [208, 128],     [232, 136], [248, 136],         [272, 128],
          [16, 144], [32, 144], [48, 144], [64, 144],                                   [128, 144],                                                 [208, 144],                                     [272, 144], [288, 144], [304, 144], [320, 144], [336, 144], [352, 144],             [384, 144], [400, 144], [416, 144], [432, 144], [448, 144], [464, 144], [480, 144], [496, 144],
                                                                                        [128, 160],                                                 [208, 152],
[0, 176], [16, 176], [32, 176], [48, 176], [64, 176], [80, 176],            [112, 176], [128, 176],
                                                                                        [128, 192],
                     [32, 208], [48, 208], [64, 208],                                   [128, 208],
                     [32, 224], [48, 224], [64, 224],                                   [128, 224],                                                                 [232, 224], [248, 224], [264, 224], [280, 224], [296, 224], [312, 224], [328, 224], [328, 224], [344, 224],
                     [32, 240],            [64, 240],                                   [128, 240],                                                                 [232, 240], [248, 240], [264, 240], [280, 240], [296, 240], [312, 240], [328, 240], [328, 240], [344, 240],
                                                                                                                                                                    [232, 256], [248, 256], [264, 256], [280, 256], [296, 256], [312, 256], [328, 256], [328, 256], [344, 256],
                     [32, 272], [48, 272], [64, 272],                                   [128, 272], [144, 272], [160, 272], [176, 272], [192, 272], [208, 272], [224, 272], [240, 272], [256, 272], [272, 272], [288, 272], [304, 272], [320, 272], [336, 272], [352, 272], [368, 272],             [400, 272], [416, 272], [432, 272], [448, 272], [464, 272], [480, 272],
                     [32, 288], [48, 288], [64, 288],                                                                                                                                                                       [304, 288],
                     [32, 304], [48, 304], [64, 304],                                   [128, 304],                                                                                                                         [304, 304],                                                                         [416, 304], [432, 304], [448, 304], [464, 304], [480, 304],
                                                                                        [128, 320],                         [176, 320], [192, 320], [208, 320],                                                             [304, 320],                                                                         [416, 320],
                                                                                        [128, 336],                         [176, 336], [192, 336], [208, 336],                                           [280, 336], [296, 336], [312, 336], [328, 336],                                                       [416, 336],                         [464, 344], [480, 344], [496, 344],
                                                                                        [128, 352],                         [176, 344], [192, 352], [208, 352],                                           [280, 352], [296, 352], [312, 352], [328, 352],                                                       [416, 352],                         [464, 352], [480, 352], [496, 352],
                                                                                        [128, 368],                                     [192, 368], [208, 368],                                                                   [312, 368], [328, 368],                                                       [416, 368], [432, 368], [448, 368], [464, 368], [480, 368], [496, 368],
                                                                                        [128, 384], [144, 384], [160, 384],             [192, 384], [208, 384],                                                                   [312, 384], [328, 384],
                                                                                                                [160, 400],             [192, 400], [208, 400],                                                                   [312, 400], [328, 400],                                                       [416, 400],
                                                                                                                [160, 416],                                                                                                       [312, 408], [328, 408],                                                       [416, 416], [432, 416], [448, 416], [464, 416], [480, 416],
                                                                                                                                                                                                                                        [320, 416],                                                             [416, 432],                         [464, 432],
                                                                                                                [160, 448],                                                                                                                                                                                     [416, 448], [432, 448],             [464, 448],             [496, 448],
                                                                                                                [160, 464],                                                                                                             [320, 448],                                                             [416, 464],                                                 [496, 464],
                                                                                                                [160, 480],                                                                                                             [320, 464],                                                             [416, 480],             [448, 480], [464, 480],
                                                                                                                [160, 496],                                                                                                             [320, 480],                                                             [416, 496],                                     [480, 496], [496, 496],
                                                                                                                                                                                                                                        [320, 496],

                                                                                        ], [

                     [32, 0  ], [48, 0  ], [64, 0  ], [80, 0  ],                                                                                                             [240, 0  ],                                                                                                [384, 0  ],                                                [464, 0  ], [480, 0  ], [496, 0  ],
                     [32, 16 ], [48, 16 ], [64, 16 ], [80, 16 ],                                                                                                                                                                                                                        [384, 16 ],                                                [464, 16 ], [480, 16 ], [496, 16 ],
          [16, 32 ], [32, 32 ], [48, 32 ], [64, 32 ], [80, 32 ], [96, 32 ], [112, 32 ], [128, 32 ],                                                                          [240, 32 ],                                                                                                [384, 32 ],                                                [464, 32 ], [480, 32 ], [496, 32 ],
                                           [64, 48 ], [80, 48 ], [96, 48 ], [112, 48 ], [128, 48 ], [144, 48 ], [160, 48 ],                          [208, 48 ], [224, 48 ], [240, 48 ], [256, 48 ], [272, 48 ],                                                                        [384, 48 ],
                     [32, 64 ],                                                                                                                      [208, 64 ], [224, 64 ],
                     [32, 80 ],                                                                                                                      [208, 80 ], [224, 80 ], [240, 80 ], [256, 80 ], [272, 80 ],                                                                        [384, 80 ],
          [16, 96 ], [32, 96 ], [48, 96 ], [64, 96 ], [80, 96 ],                                                                                                             [240, 96 ],                                                                                                [384, 96 ],
                                           [64, 112], [80, 112], [96, 112], [112, 112],                                                                                                                                                                                                 [384, 112],
                                                                 [96, 128], [112, 128],                                                 [192, 128], [208, 128], [224, 128], [240, 128], [256, 128], [272, 128], [288, 128], [304, 128], [320, 128],                                     [384, 128],
[0, 144], [16, 144], [32, 144], [48, 144], [64, 144], [80, 144], [96, 144], [112, 144], [128, 144],             [160, 144], [176, 144], [192, 144], [208, 144], [224, 144],                                                             [320, 144],                                     [384, 144],
                                                                            [112, 160],                                     [176, 160],             [208, 160],                                                                         [320, 160], [336, 160],             [368, 160], [384, 160],
          [16, 176], [32, 176],            [64, 176], [80, 176],            [112, 176],                                     [176, 176],             [208, 176],                                                                         [320, 176],                                     [384, 176],
                     [32, 192], [48, 192], [64, 192],                       [112, 192],                                     [176, 192],             [208, 192],                                                                                                                         [384, 192], [400, 192], [416, 192], [432, 192],             [464, 192], [480, 192], [496, 192],
                                                                                                                            [176, 208],             [208, 208],                                                                         [320, 208],
                                                                            [112, 224],                                     [176, 224],             [208, 224],                                                 [288, 224], [304, 224], [320, 224],
                                                                            [112, 240],                                                             [208, 240],                         [256, 240], [272, 240], [288, 240], [304, 240], [320, 240], [336, 240],
                                                                            [112, 256],                                     [176, 256],             [208, 256],                         [256, 256], [272, 256], [288, 256], [304, 256], [320, 256], [336, 256], [352, 256], [368, 256],                                                 [448, 256], [464, 256], [480, 256], [496, 256],
[0, 272],            [32, 272], [48, 272], [64, 272],            [96, 272], [112, 272], [128, 272], [144, 272], [160, 272], [176, 272], [192, 272], [208, 272], [224, 272],             [256, 272], [272, 272], [288, 272], [304, 272], [320, 272], [336, 272], [352, 272], [368, 272],                                                 [448, 272], [464, 272], [480, 272], [496, 272],
                                                                            [112, 288],                         [160, 288],                         [208, 288],                         [256, 288], [272, 288], [288, 288], [304, 288], [320, 288],                                                                                     [448, 288], [464, 288],
                                                                            [112, 304],                         [160, 304],                         [208, 304],                                                                                                                                                                         [448, 304], [464, 304], [480, 304],
                                                                            [112, 320],                                                             [208, 320],                         [256, 320],                                                                                                                                     [448, 320], [464, 320], [480, 320],
                     [32, 336], [48, 336], [64, 336],                       [112, 336],                                                                                                 [256, 336],                                                                                                                                     [448, 336], [464, 336], [480, 336],
                     [32, 352], [48, 352], [64, 352],                       [112, 352],                                                 [192, 352], [208, 352],                         [256, 352],
                                                                            [112, 368],                                                 [192, 368],                                     [256, 368],
                     [32, 384], [48, 384], [64, 384],                       [112, 384],                                                 [192, 384],                                     [256, 384],                                     [320, 384],
                     [32, 400], [48, 400], [64, 400],                       [112, 400],                                     [176, 400], [192, 400],             [224, 400], [240, 400], [256, 400],                                     [320, 400],
                                                                            [112, 416],                                     [176, 416], [192, 416],             [224, 416], [240, 416], [256, 416],                                     [320, 416],                                                                        [432, 416], [448, 416],             [480, 416], [496, 416],
                                                                            [112, 432],                         [160, 432], [176, 432], [192, 432], [208, 432], [224, 432], [240, 432],                         [288, 432], [304, 432], [320, 432],                                                                        [432, 432], [448, 432],             [480, 432], [496, 432],
                                                                            [112, 448],                                                                                                             [272, 448], [288, 448], [304, 448], [320, 448],
                                                                            [112, 464],                                                                                                 [256, 464], [272, 464], [288, 464], [304, 464], [320, 464],                                                            [416, 464], [432, 464], [448, 464],
                                                                            [112, 480],                                                                         [224, 480],             [256, 480], [272, 480], [288, 480], [304, 480], [320, 480],                                                            [416, 480], [432, 480], [448, 480],
                                                                            [112, 496],                                                 [192, 496], [208, 496], [224, 496],                                                                                                                                    [416, 496], [432, 496], [448, 496],
                                                                                        ],

                                                                                        [
[0  , 0  ]
                                                                                        ] ]



# charge les différentes images




# instance de choix de personnage
def choix_personnage(u, v) :

    if pyxel.btn(pyxel.KEY_LEFT) :
        u = 0
        v = 16
    elif pyxel.btn(pyxel.KEY_RIGHT) :
        u = 16
        v = 16
    return u, v




def joueur_deplacement(x , y, en_mouvement, direction_j) :   #Déplacement du joueur (droite, gauche, bas, haut, diagonales) et change l'image utiliser pour faire une animation

    en_mouvement = False
    if pyxel.btn(pyxel.KEY_RIGHT) or pyxel.btn(pyxel.KEY_D) :
        if (not (pyxel.btn(pyxel.KEY_DOWN) or pyxel.btn(pyxel.KEY_S))) and (not (pyxel.btn(pyxel.KEY_UP) or pyxel.btn(pyxel.KEY_Z))) :
            if ( x < x_fenetre-taille ) :
                x += v_j
                en_mouvement = True
                direction_j = "droite"
        elif pyxel.btn(pyxel.KEY_DOWN) or pyxel.btn(pyxel.KEY_S) :
            if ( x < x_fenetre-taille ) and ( y < y_fenetre-taille ) :
                x += v_j / 1.5
                y += v_j / 1.5
                en_mouvement = True
                direction_j = "droite"
        elif pyxel.btn(pyxel.KEY_UP) or pyxel.btn(pyxel.KEY_Z) :
            if ( x < x_fenetre-taille ) and ( y > 8 ) :
                x += v_j / 1.5
                y -= v_j / 1.5
                en_mouvement = True
                direction_j = "droite"

    elif pyxel.btn(pyxel.KEY_LEFT) or pyxel.btn(pyxel.KEY_Q) :
        if (not (pyxel.btn(pyxel.KEY_DOWN) or pyxel.btn(pyxel.KEY_S))) and (not (pyxel.btn(pyxel.KEY_UP) or pyxel.btn(pyxel.KEY_Z))) :
            if ( x > 0 ) :
                x -= v_j
                en_mouvement = True
                direction_j = "gauche"
        elif pyxel.btn(pyxel.KEY_DOWN) or pyxel.btn(pyxel.KEY_S) :
            if ( x > 0 ) and (y < y_fenetre-taille) :
                x -= v_j / 1.5
                y += v_j / 1.5
                en_mouvement = True
                direction_j = "gauche"
        elif pyxel.btn(pyxel.KEY_UP) or pyxel.btn(pyxel.KEY_Z) :
            if ( x > 0 ) and ( y > 8 ) :
                x -= v_j / 1.5
                y -= v_j / 1.5
                en_mouvement = True
                direction_j = "gauche"

    elif pyxel.btn(pyxel.KEY_DOWN) or pyxel.btn(pyxel.KEY_S) :
        if (y < y_fenetre-taille) :
            y += v_j
            en_mouvement = True
            direction_j = "droite"

    elif pyxel.btn(pyxel.KEY_UP) or pyxel.btn(pyxel.KEY_Z) :
        if ( y > 8 ) :
            y -= v_j
            en_mouvement = True
            direction_j = "droite"

    return x, y, en_mouvement, direction_j




# déplacement des ennemis selon leur objectifs
def ennemis_deplacement(x, y, s, i, direction_e):
    if x < targets[niveau][i][0] :
        if (x , y) != (targets[niveau][i][0], targets[niveau][i][1]) and s == 0 :
            x += v_e
            direction_e = "droite"
        if (x , y) == (targets[niveau][i][0], targets[niveau][i][1]) :
            s = 1
        if (x , y) != (ennemis_i[niveau][i][0], ennemis_i[niveau][i][1]) and s == 1 :
            x -= v_e
            direction_e = "gauche"
        if (x , y) == (ennemis_i[niveau][i][0], ennemis_i[niveau][i][1]) :
            s = 0
    elif x > targets[niveau][i][0] :
        if (x , y) != (targets[niveau][i][0], targets[niveau][i][1]) and s == 0 :
            x -= v_e
            direction_e = "gauche"
        if (x , y) == (targets[niveau][i][0], targets[niveau][i][1]) :
            s = 1
        if (x , y) != (ennemis_i[niveau][i][0], ennemis_i[niveau][i][1]) and s == 1 :
            x += v_e
            direction_e = "droite"
        if (x , y) == (ennemis_i[niveau][i][0], ennemis_i[niveau][i][1]) :
            s = 0
    elif y < targets[niveau][i][1] :
        if (x , y) != (targets[niveau][i][0], targets[niveau][i][1]) and s == 0 :
            y += v_e
            direction_e = "droite"
        if (x , y) == (targets[niveau][i][0], targets[niveau][i][1]) :
            s = 1
        if (x , y) != (ennemis_i[niveau][i][0], ennemis_i[niveau][i][1]) and s == 1 :
            y -= v_e
            direction_e = "gauche"
        if (x , y) == (ennemis_i[niveau][i][0], ennemis_i[niveau][i][1]) :
            s = 0
    elif y > targets[niveau][i][1] :
        if (x , y) != (targets[niveau][i][0], targets[niveau][i][1]) and s == 0 :
            y -= v_e
            direction_e = "gauche"
        if (x , y) == (targets[niveau][i][0], targets[niveau][i][1]) :
            s = 1
        if (x , y) != (ennemis_i[niveau][i][0], ennemis_i[niveau][i][1]) and s == 1 :
            y += v_e
            direction_e = "droite"
        if (x , y) == (ennemis_i[niveau][i][0], ennemis_i[niveau][i][1]) :
            s = 0
    return x, y, s, direction_e




# détècte si le joueur rencontre un obstacle empèchant de passer
def collision(x, y):

    for obstacle in range(len(obstacles[niveau])):
        if (obstacles[niveau][obstacle][0] <= x + taille and obstacles[niveau][obstacle][1] <= y + taille and obstacles[niveau][obstacle][0] + taille_obstacle >= x and obstacles[niveau][obstacle][1] + taille_obstacle >= y) and admin == False:

            # on reprends le code de déplacement et l'inverse pour annuler un déplacement quand un obstacle est rencontré
            if pyxel.btn(pyxel.KEY_RIGHT) or pyxel.btn(pyxel.KEY_D) :
                if (not (pyxel.btn(pyxel.KEY_DOWN) or pyxel.btn(pyxel.KEY_S))) and (not (pyxel.btn(pyxel.KEY_UP) or pyxel.btn(pyxel.KEY_Z))) :
                    if (x < x_fenetre-taille) :
                        x -= v_j
                elif pyxel.btn(pyxel.KEY_DOWN) or pyxel.btn(pyxel.KEY_S) :
                    if (x < x_fenetre-taille) and (y < y_fenetre-taille) :
                        x -= v_j / 1.5
                        y -= v_j / 1.5
                elif pyxel.btn(pyxel.KEY_UP) or pyxel.btn(pyxel.KEY_Z) :
                    if (x < x_fenetre-taille) and (y > 0) :
                        x -= v_j / 1.5
                        y += v_j / 1.5

            elif pyxel.btn(pyxel.KEY_LEFT) or pyxel.btn(pyxel.KEY_Q) :
                if (not (pyxel.btn(pyxel.KEY_DOWN) or pyxel.btn(pyxel.KEY_S))) and (not (pyxel.btn(pyxel.KEY_UP) or pyxel.btn(pyxel.KEY_Z))) :
                    if (x > 0) :
                        x += v_j
                elif pyxel.btn(pyxel.KEY_DOWN) or pyxel.btn(pyxel.KEY_S) :
                    if (x > 0) and (y < y_fenetre-taille) :
                        x += v_j / 1.5
                        y -= v_j / 1.5
                elif pyxel.btn(pyxel.KEY_UP) or pyxel.btn(pyxel.KEY_Z) :
                    if (x > 0) and (y > 0) :
                        x += v_j / 1.5
                        y += v_j / 1.5

            elif pyxel.btn(pyxel.KEY_DOWN) or pyxel.btn(pyxel.KEY_S) :
                if (y < y_fenetre-taille) :
                    y -= v_j

            elif pyxel.btn(pyxel.KEY_UP) or pyxel.btn(pyxel.KEY_Z) :
                if (y > 0) :
                    y += v_j

            return x, y
            if admin == True :
                pass
    return x, y




# détècte si l'objectif est atteint
def victoire():
    if joueur[niveau][0] <= objectif[niveau][0] + taille and joueur[niveau][1] <= objectif[niveau][1] + taille and joueur[niveau][0] + taille >= objectif[niveau][0] and joueur[niveau][1] + taille >= objectif[niveau][1] :
        return True
    else:
        return False




# détècte le contacte entre le joueur et un ennemie
def contacte_ennemie():

    if admin == False :

        # détècte la collisions sur un carré de 48px entre le joueur et les ennemis
        for ennemi in range(len(ennemis[niveau])):

            if ennemis[niveau][ennemi][0] - taille < joueur[niveau][0] + taille * 3 and ennemis[niveau][ennemi][1] - taille < joueur[niveau][1] + taille * 3 and ennemis[niveau][ennemi][0] + taille * 2 > joueur[niveau][0] - taille * 2 and ennemis[niveau][ennemi][1] + taille * 2 > joueur[niveau][1] - taille * 2 :
                return True
    if admin == True :
        return False
    return False

    # détècte la collision sur un rayon de 20px entre le joueur et les ennemis
    """if admin == False :
        for ennemi in range(len(ennemis)) :

            if joueur[niveau][0] >= ennemis[niveau][ennemi][0] :
                s = joueur[niveau][0] - ennemis[niveau][ennemi][0]

                if joueur[niveau][1] >= ennemis[niveau][ennemi][1] :
                    s += joueur[niveau][1] - ennemis[niveau][ennemi][1]

                if joueur[niveau][1] <= ennemis[niveau][ennemi][1] :
                    s += ennemis[niveau][ennemi][1] - joueur[niveau][1]

            elif joueur[niveau][0] <= ennemis[niveau][ennemi][0] :
                s = ennemis[niveau][ennemi][0] - joueur[niveau][0]

                if joueur[niveau][1] >= ennemis[niveau][ennemi][1] :
                    s += joueur[niveau][1] - ennemis[niveau][ennemi][1]

                if joueur[niveau][1] <= ennemis[niveau][ennemi][1] :
                    s += ennemis[niveau][ennemi][1] - joueur[niveau][1]

            if s <= taille * 5 :
                return True

    if admin == True :
        return False
    return False"""




# détècte si un bonus est récupérez
def bonus_recuperer(bonus_recup, total_b) :
    for b in range(len(bonus[niveau])) :

        if bonus[niveau][b][0] <= joueur[niveau][0] + taille and bonus[niveau][b][1] <= joueur[niveau][1] + taille and bonus[niveau][b][0] + taille >= joueur[niveau][0] and bonus[niveau][b][1] + taille >= joueur[niveau][1] :

            if bonus_recup[niveau][b] == False :

                bonus_recup[niveau][b] = True
                total_b[niveau][0] += 1


            return bonus_recup, total_b
    return bonus_recup, total_b



# censé faire défilé le texte petit à petit pour effet de dialogue mais n'est pas finalisé
def defilement_texte(texte,x,y):
    decalage = 2
    for i in texte:
        pyxel.text(x, y, i, 7)
        x += decalage




# =========================================================
# == UPDATE
# =========================================================
def update():
    """mise à jour des variables (30 fois par seconde)"""


    global u, v, menu, menu_demarrage, menu_niveau, nb_niveau, niveau, v_e, objectifs_atteint, admin, direction_e, direction_j, en_mouvement, bonus_recup, total_b, decor, dialogue_n, dialogue_tuto_confirmations, speedrun_mode, meilleur_temps, t_start, t_end, timer_start, timer_end


    if menu_demarrage == True :

        if ((u == 0 and v == 16) or ( u == 16 and v == 16 )) and menu_niveau == False :

            if pyxel.btn(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 113 + 28 and pyxel.mouse_y <= 85 + 15 and pyxel.mouse_x >= 113 and pyxel.mouse_y >= 85) :

                v_e = 0
                menu_demarrage = False
                joueur[niveau][0], joueur[niveau][1] = joueur_x[niveau], joueur_y[niveau]
                t_start = False
                t_end = False


            if pyxel.btn(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 81 + 91 and pyxel.mouse_y <= 105 + 15 and pyxel.mouse_x >= 81 and pyxel.mouse_y >= 105) :

                u, v = 0, 0


            if pyxel.btn(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 85 + 83 and pyxel.mouse_y <= 125 + 15 and pyxel.mouse_x >= 85 and pyxel.mouse_y >= 125) :

                menu_niveau = True


            if pyxel.btnr(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 98 + 59 and pyxel.mouse_y <= 145 + 15 and pyxel.mouse_x >= 98 and pyxel.mouse_y >= 145) :

                if speedrun_mode == False :

                    speedrun_mode = True


                elif speedrun_mode == True :

                    speedrun_mode = False


            if pyxel.btnr(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 108 + 38 and pyxel.mouse_y <= 200 + 15 and pyxel.mouse_x >= 108 and pyxel.mouse_y >= 200) :

                pyxel.quit()



        # séquence de choix de personnage
        if u == 0 and v == 0 :
            u, v = choix_personnage(u, v)

        # séquence de visionnage niveau
        if menu_niveau == True :

            for i in range(nb_niveau):


                if objectifs_atteint[i] == True :

                    s = 0 + 17 * i


                    if pyxel.btn(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 0 + 50 and pyxel.mouse_y <= s + 16 and pyxel.mouse_x >= 0 and pyxel.mouse_y >= s) :

                        niveau = i
                        t_start = False
                        t_end = False


                    if niveau == i:

                        if pyxel.btn(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 142 + 27 and pyxel.mouse_y <= 215 + 15 and pyxel.mouse_x >= 142 and pyxel.mouse_y >= 215) :

                            v_e = 0
                            menu_niveau = False
                            menu_demarrage = False
                            joueur[niveau][0], joueur[niveau][1] = joueur_x[niveau], joueur_y[niveau]

                            for ennemi in range(len(ennemis[niveau])):

                                ennemis[niveau][ennemi][0], ennemis[niveau][ennemi][1] = ennemis_i[niveau][ennemi][0], ennemis_i[niveau][ennemi][1]
                            t_start = False
                            t_end = False


                if objectifs_atteint[i] == False :

                    pass # normal


            if pyxel.btn(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 125 + 63 and pyxel.mouse_y <= 235 + 15 and pyxel.mouse_x >= 125 and pyxel.mouse_y >= 235) :

                    menu_niveau = False



    if menu_demarrage == False :



        #séquence de choix de difficulté
        if v_e == 0 :

            if pyxel.btn(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 104 + 46 and pyxel.mouse_y <= 95 + 15 and pyxel.mouse_x >= 104 and pyxel.mouse_y >= 95) :

                v_e = 1


            if pyxel.btn(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 102 + 50 and pyxel.mouse_y <= 115 + 15 and pyxel.mouse_x >= 102 and pyxel.mouse_y >= 115) :

                v_e = 2


            if pyxel.btn(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 108 + 38 and pyxel.mouse_y <= 135 + 15 and pyxel.mouse_x >= 108 and pyxel.mouse_y >= 135) :

                v_e = 3



        if v_e >= 1 and v_e <= 3 :


            if niveau == 0 :

                dialogue_tuto_confirmations[0] = dialogue_tuto[dialogue_n]


                if dialogue_tuto_confirmations[0] != dialogue_tuto[5] :


                    if pyxel.btnr(pyxel.KEY_SPACE):

                        dialogue_n += 1


            if niveau == 1 :

                dialogue_tuto_confirmations[0] = dialogue_niveau2[dialogue_n]

                if dialogue_tuto_confirmations[0] != dialogue_niveau2[2] :

                    if pyxel.btnr(pyxel.KEY_SPACE):

                        dialogue_n += 1


            if niveau == 2 :

                dialogue_tuto_confirmations[0] = dialogue_niveau3[dialogue_n]

                if dialogue_tuto_confirmations[0] != dialogue_niveau2[2] :

                    if pyxel.btnr(pyxel.KEY_SPACE):

                        dialogue_n += 1


            if dialogue_tuto_confirmations[0] == dialogue_tuto[5] or dialogue_tuto_confirmations[0] == dialogue_niveau2[2] or dialogue_tuto_confirmations[0] == dialogue_niveau2[2] :


                if speedrun_mode == True :

                    if t_start == False :
                        timer_start = time.time()
                        t_start = True


                # détection d'une collision entre ennemi et personnage
                contacte_ennemie()


                # détection d'une collision entre l'objectif et le personnage
                victoire()



                # détècte si échap est ouvert pour ouvrir menu
                if pyxel.btn(pyxel.KEY_P) :
                    menu = True



                # active le menu
                if menu == True:

                    if pyxel.btnr(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 106 + 46 and pyxel.mouse_y <= 95 + 15 and pyxel.mouse_x >= 106 and pyxel.mouse_y >= 95) :

                        menu = False


                    if pyxel.btnr(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 82 + 91 and pyxel.mouse_y <= 115 + 15 and pyxel.mouse_x >= 82 and pyxel.mouse_y >= 115) :

                        menu = False
                        joueur[niveau][0], joueur[niveau][1] = joueur_x[niveau], joueur_y[niveau]

                        for ennemi in range(len(ennemis[niveau])):

                            ennemis[niveau][ennemi][0], ennemis[niveau][ennemi][1] = ennemis_i[niveau][ennemi][0], ennemis_i[niveau][ennemi][1]


                        v_e = 0
                        t_start = False
                        t_end = False


                    if pyxel.btn(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 102 + 50 and pyxel.mouse_y <= 135 + 15 and pyxel.mouse_x >= 102 and pyxel.mouse_y >= 135) :

                        joueur[niveau][0], joueur[niveau][1] = joueur_x[niveau], joueur_y[niveau]
                        for ennemi in range(len(ennemis[niveau])):

                            ennemis[niveau][ennemi][0], ennemis[niveau][ennemi][1] = ennemis_i[niveau][ennemi][0], ennemis_i[niveau][ennemi][1]

                        menu = False
                        t_start = False
                        t_end = False


                    if pyxel.btnr(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 108 + 38 and pyxel.mouse_y <= 200 + 15 and pyxel.mouse_x >= 108 and pyxel.mouse_y >= 200) :

                        menu = False
                        menu_demarrage = True
                        t_start = False
                        t_end = False



                # séquence se répétant quand le jeu est en route
                if  (contacte_ennemie() == False) and (victoire() == False) and (menu == False):

                    # appelle la vérification de contacte avec un point bonus
                    bonus_recup, total_b, = bonus_recuperer(bonus_recup, total_b)


                    # mise à jour de la position du personnage
                    joueur[niveau][0], joueur[niveau][1], en_mouvement, direction_j = joueur_deplacement(joueur[niveau][0], joueur[niveau][1], en_mouvement, direction_j)


                    #suivi du personnage
                    camera[0], camera[1] = joueur[niveau][0] - 124 , joueur[niveau][1] - 124


                    # mise à jour de la position des ennemis
                    global etat

                    for i in range(len(targets[niveau])) :

                        ennemis[niveau][i][0], ennemis[niveau][i][1], etat[niveau][i], direction_e[niveau][i] = ennemis_deplacement(ennemis[niveau][i][0], ennemis[niveau][i][1], etat[niveau][i], i, direction_e[niveau][i])


                    # contacte avec un obstacle
                    joueur[niveau][0], joueur[niveau][1] = collision(joueur[niveau][0], joueur[niveau][1])



                # permet de relancer ou de quitte le jeu quand le joueur gagne ou perd
                if ((contacte_ennemie() or victoire()) == True) :

                    for i in range(nb_niveau) :

                        if victoire() == True :


                            if speedrun_mode == True :

                                if t_end == False :
                                    timer_end = time.time()
                                    t_end = True


                                elif t_end == True :

                                    if meilleur_temps[niveau] > (timer_start - timer_end) :

                                        meilleur_temps[niveau] = -(timer_start - timer_end)


                            niveau_save = str(niveau)
                            objectifs_atteint_save = str(objectifs_atteint)
                            bonus_recup_save = str(bonus_recup)
                            total_b_save = str(total_b)

                            sauvegarde = open("../Sources/save.txt", "w")
                            sauvegarde.write(niveau_save + "\n" + objectifs_atteint_save +"\n" + bonus_recup_save + "\n" + total_b_save)
                            sauvegarde.close


                            if pyxel.btnr(pyxel.KEY_SPACE) :

                                niveau += 1
                                t_start = False
                                t_end = False
                                dialogue_n = 0


                            if niveau == i :

                                objectifs_atteint[i+1] = True



                        if pyxel.btnr(pyxel.KEY_SPACE) :

                            joueur[niveau][0], joueur[niveau][1] = joueur_x[niveau], joueur_y[niveau]
                            for ennemi in range(len(ennemis[niveau])):

                                ennemis[niveau][ennemi][0], ennemis[niveau][ennemi][1] = ennemis_i[niveau][ennemi][0], ennemis_i[niveau][ennemi][1]


                        if pyxel.btnr(pyxel.MOUSE_BUTTON_LEFT) and (pyxel.mouse_x <= 108 + 38 and pyxel.mouse_y <= 200 + 15 and pyxel.mouse_x >= 108 and pyxel.mouse_y >= 200) :

                            menu_demarrage = True
                            joueur[niveau][0], joueur[niveau][1] = joueur_x[niveau], joueur_y[niveau]
                            t_start = False
                            t_end = False
                            dialogue_n = 0



# =========================================================
# == DRAW
# =========================================================
def draw():
    """création des objets (30 fois par seconde)"""


    # vide la fenetre
    pyxel.cls(0)

    pyxel.sound(10)
    # désactive l'affichage de la souris
    pyxel.mouse(False)


    if menu_demarrage == True :

        if ((u == 0 and v == 16) or ( u == 16 and v == 16 )) and menu_niveau == False :

            # utilisation de la tilemap pour faire un fond au menu
            pyxel.bltm(0, 0, 0, 1792, 1792, 256, 256)

            pyxel.mouse(True)
            pyxel.camera(x_camera, y_camera)

            pyxel.text(110, 32, "BlindMaze", 7)

            pyxel.rectb(113, 85, 27, 15, 1)
            pyxel.rect(114, 86, 25, 13, 0)
            pyxel.text(117, 90, "JOUER", 7)

            pyxel.rectb(81, 105, 91, 15, 1)
            pyxel.rect(82, 106, 89, 13, 0)
            pyxel.text(85, 110, "CHOISIR UN PERSONNAGE", 7)

            pyxel.rectb(85, 125, 83, 15, 1)
            pyxel.rect(86, 126, 81, 13, 0)
            pyxel.text(89, 130, "SELECTIONNER NIVEAU", 7)


            if speedrun_mode == False :

                pyxel.rect(99, 146, 57, 13, 8)


            if speedrun_mode == True :

                pyxel.rect(99, 146, 57, 13, 11)


            pyxel.rectb(98, 145, 59, 15, 1)
            pyxel.text(102, 150, "SPEEDRUN MODE", 7)


            pyxel.rectb(108, 200, 38, 15, 8)
            pyxel.rect(109, 201, 36, 13, 0)
            pyxel.text(113, 205, "QUITTER", 7)

            pyxel.text(1, 250, "P : POUR METTRE EN PAUSE", 7)


        # affichage d'écran de choix de personnage
        if (u, v) == (0, 0):

            pyxel.text(75, 32, "SELECTIONNEZ UN PERSONNAGE !", 7)

            pyxel.rectb(32, 80, 64, 64, 1)
            pyxel.blt(60, 107, 0, 0, 16, 8, 8, 2)

            pyxel.rectb(160, 80, 64, 64, 1)
            pyxel.blt(188, 107, 0, 16, 16, 8, 8, 2)

            pyxel.tri(45, 195, 83, 174, 83, 214, 7)
            pyxel.tri(211, 195, 173, 174, 173, 214, 7)
            pyxel.text(126, 192, "OU", 7)


        if menu_niveau == True :

            pyxel.mouse(True)

            pyxel.line(50, 0, 50, 256, 7)

            for i in range(nb_niveau):

                if objectifs_atteint[i] == True :

                    s = 0 + 17 * i
                    text_n = "NIVEAU " + str(i + 1)
                    pyxel.rect(0, s, 50, 16, 1)
                    pyxel.line(0, s + 16, 50, s + 16, 7)
                    pyxel.text(5, s + 5, text_n, 7)

                    if niveau == i :
                        pyxel.rect(0, s, 50, 16, 2)
                        pyxel.text(5, s + 5, text_n, 7)
                        text_b = "bonus recuperer : " + str(total_b[i][0]) + " / " + str(len(bonus[i]))
                        pyxel.text(140, 15, text_n, 7)

                        text_n1 = "niveau_" + str(i + 1)
                        text_png ="../Sources/" + text_n1 + ".png"
                        pyxel.image(2).load(0, 0, text_png)
                        pyxel.rectb(79, 37, 150, 150, 1)
                        pyxel.blt(80, 38, 2, 0, 0, 148, 148)

                        pyxel.text(79, 190, text_b, 7)

                        if speedrun_mode == True :

                            text_time = str(meilleur_temps[niveau])
                            pyxel.text(79, 200, "meilleur temps : " + text_time, 7)

                if objectifs_atteint[i] == False :
                    pass # normal


            pyxel.rectb(142, 215, 27, 15, 1)
            pyxel.text(146, 220, "JOUER", 7)

            pyxel.rectb(125, 235, 63, 15, 1)
            pyxel.text(129, 240, "RETOUR AU MENU", 7)




    if menu_demarrage == False :



        if v_e == 0 :

            pyxel.mouse(True)
            pyxel.camera(x_camera, y_camera)

            pyxel.text(80, 32, "CHOISISSEZ LA DIFFICULTE", 7)

            pyxel.rectb(111, 95, 31, 15, 11)
            pyxel.text(115, 100, "FACILE", 7)

            pyxel.rectb(109, 115, 35, 15, 10)
            pyxel.text(113, 120, "NORMALE", 7)

            pyxel.rectb(106, 135, 43, 15, 8)
            pyxel.text(110, 140, "DIFFICILE", 7)



        if v_e >= 1 and v_e <= 3:

            if niveau == 0 :

                if dialogue_tuto_confirmations[0] != dialogue_tuto[5] :

                    pyxel.camera(1792,1472)
                    pyxel.bltm(1792, 1600, niveau, 1792, 1600, 256, 128)
                    #defilement_texte(dialogue_tuto_confirmations[0], 1812, 1652)
                    pyxel.text(1812,1652, dialogue_tuto_confirmations[0], 7)


            if niveau == 1 :

                if dialogue_tuto_confirmations[0] != dialogue_niveau2[2] :

                    pyxel.camera(1792,1472)
                    pyxel.bltm(1792, 1600, niveau, 1792, 1600, 256, 128)
                    pyxel.text(1812,1652, dialogue_tuto_confirmations[0], 7)


            if niveau == 2 :

                if dialogue_tuto_confirmations[0] != dialogue_niveau2[2] :

                    pyxel.camera(1792,1472)
                    pyxel.bltm(1792, 1600, niveau, 1792, 1600, 256, 128)
                    pyxel.text(1812,1652, dialogue_tuto_confirmations[0], 7)


            if dialogue_tuto_confirmations[0] == dialogue_tuto[5] or dialogue_tuto_confirmations[0] == dialogue_niveau2[2] :


                pyxel.playm(0,120,True)

                # actualisation de la camera
                pyxel.camera(camera[0], camera[1])


                # aucun ennemis à proximité
                if  contacte_ennemie() == False and victoire() == False and menu == False :

                    #pyxel.bltm(0, 0, niveau, 0, 0, x_fenetre, y_fenetre)



                    # chargement des morceaux de tilemap
                        #ennemis
                    for ennemi in range(len(ennemis[niveau])):

                        pyxel.bltm(ennemis[niveau][ennemi][0] - taille * 2, ennemis[niveau][ennemi][1] - taille * 2, niveau, ennemis[niveau][ennemi][0] - taille * 2, ennemis[niveau][ennemi][1] - taille * 2, taille_tile * 5, taille_tile * 5)

                        """for i in range(20):
                        pyxel.circb(ennemis[ennemi][0] + taille / 2,ennemis[ennemi][1] + taille / 2, 20 + i - 0.9, 0)"""

                        #joueur
                    pyxel.bltm(joueur[niveau][0] - taille * 2, joueur[niveau][1] - taille * 2, niveau, joueur[niveau][0] - taille * 2, joueur[niveau][1] - taille * 2, taille_tile * 5, taille_tile * 5)



                    # Bonus
                    for i in range(len(bonus[niveau])) :

                        if bonus_recup[niveau][i] == False :

                            for ennemi in range(len(ennemis[niveau])) :

                                if ((ennemis[niveau][ennemi][0] - taille * 2 < bonus[niveau][i][0] + taille and ennemis[niveau][ennemi][1] - taille * 2 < bonus[niveau][i][1] and ennemis[niveau][ennemi][0] + taille * 3 > bonus[niveau][i][0] and ennemis[niveau][ennemi][1] + taille * 3 > bonus[niveau][i][1]) or (joueur[niveau][0] - taille * 2 < bonus[niveau][i][0] + taille and joueur[niveau][1] - taille * 2 < bonus[niveau][i][1] and joueur[niveau][0] + taille * 3 > bonus[niveau][i][0] and joueur[niveau][1] + taille * 3 > bonus[niveau][i][1])) :

                                    if (pyxel.frame_count % 30 <= 9) and (pyxel.frame_count % 30 >= 0) :

                                        pyxel.blt(bonus[niveau][i][0], bonus[niveau][i][1], 0, 8, 32, 8, 8, 13)


                                    if (pyxel.frame_count % 30 <= 19) and (pyxel.frame_count % 30 >= 10) :

                                        pyxel.blt(bonus[niveau][i][0], bonus[niveau][i][1], 0, 16, 32, 8, 8, 13)


                                    if (pyxel.frame_count % 30 <= 29) and (pyxel.frame_count % 30 >= 20) :

                                        pyxel.blt(bonus[niveau][i][0], bonus[niveau][i][1], 0, 24, 32, 8, 8, 13)

                        if bonus_recup[niveau][i] == True :
                            pass





                    # joueur (8x8px)
                    if direction_j == "droite":

                        if en_mouvement == True :

                            if (pyxel.frame_count % 30 <= 14) and (pyxel.frame_count % 30 >= 0) :

                                pyxel.blt(joueur[niveau][0], joueur[niveau][1], 0, u, v, 8, 8, 2)


                            if pyxel.frame_count % 30 <= 29 and (pyxel.frame_count % 30 >= 15) :

                                pyxel.blt(joueur[niveau][0], joueur[niveau][1], 0, u + 8, v, 8, 8, 2)


                        if en_mouvement == False :

                            pyxel.blt(joueur[niveau][0], joueur[niveau][1], 0, u, v, 8, 8, 2)


                    if direction_j == "gauche" :

                        if en_mouvement == True :

                            if (pyxel.frame_count % 30 <= 14) and (pyxel.frame_count % 30 >= 0) :

                                pyxel.blt(joueur[niveau][0], joueur[niveau][1], 0, u, v - 8, 8 , 8, 2)


                            if pyxel.frame_count % 30 <= 29 and (pyxel.frame_count % 30 >= 15) :

                                pyxel.blt(joueur[niveau][0], joueur[niveau][1], 0, u + 8, v - 8, 8, 8, 2)


                        if en_mouvement == False :

                            pyxel.blt(joueur[niveau][0], joueur[niveau][1], 0, u, v - 8, 8, 8, 2)



                    # ennemies
                    for ennemi in range(len(ennemis[niveau])):

                        if direction_e[niveau][ennemi] == "droite" :

                            if (pyxel.frame_count % 30 <= 14) and (pyxel.frame_count % 30 >= 0) :

                                pyxel.blt(ennemis[niveau][ennemi][0], ennemis[niveau][ennemi][1], 0, 0, 24, 8, 8, 13)


                            if pyxel.frame_count % 30 <= 29 and (pyxel.frame_count % 30 >= 15) :

                                pyxel.blt(ennemis[niveau][ennemi][0], ennemis[niveau][ennemi][1], 0, 8, 24, 8, 8, 13)



                        if direction_e[niveau][ennemi] == "gauche" :

                            if (pyxel.frame_count % 30 <= 14) and (pyxel.frame_count % 30 >= 0) :

                                pyxel.blt(ennemis[niveau][ennemi][0], ennemis[niveau][ennemi][1], 0, 16, 24, 8, 8, 13)


                            if pyxel.frame_count % 30 <= 29 and (pyxel.frame_count % 30 >= 15) :

                                pyxel.blt(ennemis[niveau][ennemi][0], ennemis[niveau][ennemi][1], 0, 24, 24, 8, 8, 13)



                    # Objectif

                    if (joueur[niveau][0] - taille * 2 <= objectif[niveau][0] + taille and joueur[niveau][1] - taille * 2 <= objectif[niveau][1] + taille and joueur[niveau][0] + taille * 3 >= objectif[niveau][0] and joueur[niveau][1] + taille * 3 >= objectif[niveau][1]) :


                        if (pyxel.frame_count % 30 <= 6) and (pyxel.frame_count % 30 >= 0) :

                            pyxel.blt(objectif[niveau][0], objectif[niveau][1], 0, 0, 0, taille, taille, 13)


                        if (pyxel.frame_count % 30 <= 14) and (pyxel.frame_count % 30 >= 7) :

                            pyxel.blt(objectif[niveau][0], objectif[niveau][1], 0, 8, 0, taille, taille, 13)


                        if (pyxel.frame_count % 30 <= 22) and (pyxel.frame_count % 30 >= 15) :

                            pyxel.blt(objectif[niveau][0], objectif[niveau][1], 0, 16, 0, taille, taille, 13)


                        if (pyxel.frame_count % 30 <= 29) and (pyxel.frame_count % 30 >= 23) :

                            pyxel.blt(objectif[niveau][0], objectif[niveau][1], 0, 24, 0, taille, taille, 13)


                    #pyxel.bltm(0, 0, niveau, 0, 0, x_fenetre, y_fenetre) # pour screenshot

                    # Obstacles
                    """for obstacle in range(len(obstacles[niveau])):
                        pyxel.rect(obstacles[niveau][obstacle][0], obstacles[niveau][obstacle][1], 16, 16, 7)"""


                # Menu
                if menu == True :

                    pyxel.mouse(True)
                    pyxel.camera(x_camera, y_camera)

                    pyxel.text(120, 32, "MENU", 7)

                    pyxel.rectb(106, 95, 43, 15, 1)
                    pyxel.text(110, 100, "REPRENDRE", 7)

                    pyxel.rectb(82, 115, 91, 15, 1)
                    pyxel.text(86, 120, "CHANGEZ LA DIFFICULTE", 7)

                    pyxel.rectb(102, 135, 51, 15, 1)
                    pyxel.text(106, 140, "RECOMMENCER", 7)

                    pyxel.rectb(108, 200, 38, 15, 8)
                    pyxel.text(113, 205, "QUITTER", 7)



                # Game Over
                elif contacte_ennemie() == True and admin == False:

                    pyxel.mouse(True)
                    pyxel.camera(x_camera, y_camera)

                    pyxel.text(110, 90, "GAME OVER" , 8)

                    pyxel.text(60, 120, "APPUYER SUR ESPACE POUR REESSAYER", 7)

                    pyxel.rectb(108, 200, 38, 15, 8)
                    pyxel.text(113, 205, "QUITTER", 7)



                # Victoire
                elif victoire() == True:

                    pyxel.mouse(True)
                    pyxel.camera(x_camera, y_camera)

                    pyxel.text(110, 90, "VICTOIRE" , 10)

                    pyxel.text(60, 120, "APPUYER SUR ESPACE POUR CONTINUER", 7)

                    if speedrun_mode == True :

                        text_time = str(meilleur_temps[niveau])
                        pyxel.text(95, 160, text_time, 7)


                    pyxel.rectb(108, 200, 38, 15, 8)
                    pyxel.text(113, 205, "QUITTER", 7)

pyxel.run(update, draw)